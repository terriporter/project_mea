<?php
	include "header.php";
	include "config/init.php";
	include "nav.php";
	include "functions/clean_function.php";


	if(isset($_POST["submit"])){
		$name = $_POST["name"];
		$email = $_POST["email"];
		$telephone = $_POST["telephone"];
		$subject = $_POST["subject"];
		$message = $_POST["message"];

		filter_var($email, FILTER_SANITIZE_EMAIL);

		$name = clean_input($name);
		$email = clean_input($email);
		$telephone = clean_input($telephone);
		$subject = clean_input($subject);
		$message = clean_input($message);

		if (!empty($name)) {
			
			if (!empty($email)) {

				if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
					
					if (!empty($telephone)) {
						
						if (!empty($subject)) {
							
							if (!empty($message)) {

								$mail_subject = "Your enquiry about: ".$subject;
								$mail_message = "Thank you ".$name." for contacting us. Here is a copy of your enquiry: ".$message." We will contact you using the details you have given us. Telephone:  ".$telephone." and email: ".$email.". If these details are incorrect, please get back in contact with us."; 

								mail($email, $mail_subject, $mail_message);
								?>

								<div class="sixteen columns">
									<h2>Thank you! </h2>
									<p>We have noted your query, and an email has been sent to you with a copy. </p> 

								</div>

								<?php

								$iq = $handler->prepare("INSERT INTO charity_contactus (name, email, telephone, subject, message) VALUES ('".$name."', '".$email."', '".$telephone."', '".$subject."','".$message."')");
								$iq->execute();

							} else {
								$_SESSION['email_error'] = "Please input a Message.";
							}
							
						} else {
							$_SESSION['email_error'] = "Please input a Subject.";
						}
						
					} else {
						$_SESSION['email_error'] = "Please input your Telephone Number.";
					}
					
				} else {
					$_SESSION['email_error'] = "Please enter a valid Email such as name@something.com.";
				}
				
			} else {
				$_SESSION['email_error'] = "Please input your Email.";
			}
			
		} else {
			$_SESSION['email_error'] = "Please input your Name.";
		}
		
	} else {
		echo "You did not click submit";
	}

	if($_SESSION['email_error'] == "Please press submit once you have filled in the form."){

		//resetting the session
		session_unset();

	 } else {
	 	//echo "returned";
	 	header ('location: contact.php');
	 } 

?>