<!doctype html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <title>Production Project</title>
  <meta name="description" content="Level 6 Project"/>
  <meta name="author" content="Terri Porter"/>


  <meta name="viewport" content="width=device-width, initial-scale=1"/>

<!-- Setting up the font to be used on the website, multiple sizes chosen, ready for text to be different-->
<link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700,800,900" rel="stylesheet"> 

  <link rel="stylesheet" href="assets/css/main.css"/>
  <link rel="stylesheet" href="assets/css/normalize.css"/>
  <link rel="stylesheet" href="assets/css/skeleton.css"/>

   <script src="http://code.jquery.com/jquery-latest.min.js"></script>
  
  <!-- Favicon  
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="assets/images/favicon.png"/>

<!-- Google Recaptcha-->
<script src='https://www.google.com/recaptcha/api.js'></script>

</head>

<body>


  


  