<?php 

include "header.php";
include "nav.php";
include "config/init.php";
include "functions/charity_info.php";

?>

  <div class="container">
    <h2> Contact Us </h2>

    <p>If you would like to contact us for any details, please fill in the form below and we will try to contact you as soon as possible.</p>

      <form class="contact_form" action="contactformresponse.php" method="post" name="contact_form">
        <label for="name">Name:</label>
        <input type="text"  name="name" />
        <label for="email">Email:</label>
        <input type="text" name="email" />
        <span class="form_hint">Format: "name@something.com"</span>
        <label for="telephone">Telephone:</label>
        <input type="text" name="telephone" />
        <label for="subject">Subject:</label>
        <input type="text" name="subject" />
        <span class="subject_hint">Example: Membership Info"</span>
        <label for="message">Message:</label>
        <textarea name="message" cols="40" rows="4"></textarea>
      </br>
        <button class="submit" name="submit" type="submit">Submit Form</button>
       </form> 
 

      <?php
      if(!isset($_SESSION['email_error'])){
        $_SESSION["email_error"] = "Please press submit once you have filled in the form.";
      } 
      echo "<p>".$_SESSION['email_error']."</p>";

      ?>    
 </div> 

<?php
//include "footer.php";
?>