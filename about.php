<?php 

include "header.php";
include "nav.php";
include "config/init.php";
include "functions/charity_info.php";

  ?>
  <div class="container">

    <h2>About Us</h2>

    <?php 
    
    $charity_info = new charity_info;
		
		$info_array = $charity_info->charity_details($handler);

		for ($i=0; $i < count($info_array); $i++){
      echo '<p> We are one of 35 Neighbourhood Network Schemes around Leeds. Our aim is '.$info_array[$i][2].'. We provide a diverse range of services. We give practical help where needed and help people by signposting them to other organisations or places where they can get the help they need. 
      If you are not in our area, then please do not worry. You might just have to contact a different Neighbourhood Network, please <a href="https://www.leeds.gov.uk/docs/Neighbourhood%20Network%20Schemes.pdf">click here</a> where you will be able to see all their contact details and areas. </p>';
			echo '<p> If you would like to visit us, our address is: '.$info_array[$i][7].', '.$info_array[$i][8].', '.$info_array[$i][9].', '.$info_array[$i][10].'</p>';
		}

?>
    <div id="map"></div>
    <script>
      function initMap() {
        var uluru = {lat: 53.7440886, lng: -1.6027226999999584};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 17,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map,
          title: 'Our Office'
        });

        var trafficLayer = new google.maps.TrafficLayer();
        trafficLayer.setMap(map);

      }
    </script>

    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD1wwuRFRZ8oesYoNt6XGgSiYuYEoxvxq0&callback=initMap">
    </script>

  </div>


<?php
//include "footer.php";
?>